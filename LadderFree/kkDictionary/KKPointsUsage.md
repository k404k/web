###### 玩转金刚梯>金刚字典>
### 积分用途
- [ 积分 ](https://kk404.gitlab.io/web/kkpoints)可用于：
  - 在[ 金刚网 ](https://kk404.gitlab.io/web/kksitecn)购买除积分之外的所有商品
  - 在[ 积分交易所 ](https://kk404.gitlab.io/web/kkpointexchange)出售以变现
  - 当[ 金刚公司 ](https://kk404.gitlab.io/web/list_kk)[ 回购积分 ](https://kk404.gitlab.io/web/buybackkkpoints)时，出售[积分](https://kk404.gitlab.io/web/kkpoints)给[ 金刚公司 ](https://kk404.gitlab.io/web/list_kk)以变现
  - 钞票放大器：[ 购买积分 ](https://kk404.gitlab.io/web/purchasekkpoints)，使您的金钱增值
    - 比如每购买$100的积分，再用积分购买[流量](https://kk404.gitlab.io/web/kkdatatraffic)时，您的$100就变成了$120或更多

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

