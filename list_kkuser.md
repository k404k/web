###### 金刚梯>金刚帮助>

### 金刚用户类
- [何谓金刚用户？](/kkuser.md)
- [如何成为金刚用户？](/tobekkuser.md)
- [从未使用过金刚，我该如何开始？](/tobekkuser.md)
- [如何解除金刚用户身份？](/dismisskkuseridentity.md)
- [如何解除与金刚的合约？](/dismisskkuseridentity.md)
- [金刚用户与金刚网是什么关系？](/mappingrelationshipbetweenkkuser&kksitecn.md)


#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
