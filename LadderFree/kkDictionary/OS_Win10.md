###### 玩转金刚梯>金刚字典>
### Win10 操作系统

- 所谓<strong> Win10 </strong>是[ Windows ](/LadderFree/kkDictionary/Windows.md)诸多版本中的一个版本
- <strong> Win10 </strong> 是仅可安装在个人电脑(即PC)上的[ 操作系统 ](/LadderFree/kkDictionary/OS.md)
- <strong> Win10 </strong> 是美国微软公司的产品

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

