###### 玩转金刚梯>金刚字典>
### 积分账户

- 每一位[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)
  - 在于[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)[ 注册 ](/LadderFree/kkDictionary/Registration.md)成功或
  - 在[ 金刚App梯 ](/LadderFree/kkDictionary/KKLadderAPP.md)安装成功的同时
- 其名下都被开立了一个专门用于记载[ 积分 ](/LadderFree/kkDictionary/KKPoints.md)收支情况的账户
- 该账户即是<strong> 积分账户 </strong>

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



