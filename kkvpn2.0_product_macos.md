#### 金刚梯>金刚帮助>金刚2.0app梯>
### 产品获取
- 适用于 苹果电脑MacOS

- 特色
  - 一键翻墙
  - 安全高速 
  - 多条线路 
  - 免费公测 
  - HTTPS/SSL VPN

- 获取
  - 研发中，请耐心等待...

#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚公司类](/list_kk.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
- [金刚2.0app梯产品获取与安装](/list_kkproducts2.0.md)
